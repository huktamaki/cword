var colors = {
  white: [255, 255, 255],
  blue: [65, 56, 155],
  black: [0, 0, 0]
}

var intro = null

var texts = {
  font: [],
  note: 'We have your freedom\nGive us your whole\nlifetime and we may\nprovide you with the\nbare basics of living',
  desc: 'A typeface for writing ransom notes.\nOr for whatever purpose of yours.',
  down: 'DOWNLOAD FONT'
}

var states = {
  intro: true,
  outro: false
}

var board = {
  x: null,
  y: null,
}

function preload() {
  intro = loadImage('img/word.jpg')
  texts.font.push(loadFont('ttf/corporate_fonts.ttf'))
  texts.font.push(loadFont('ttf/source_sans_pro.ttf'))
}

function setup() {
  if (windowWidth >= windowHeight * (16 / 9)) {
    board.x = windowHeight * (16 / 9)
    board.y = windowHeight
  } else {
    board.x = windowWidth
    board.y = windowWidth * (9 / 16)
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  background(colors.blue)
  rectMode(CENTER)
  frameRate(60)

  if (states.intro === true) {
    image(intro, (windowWidth - board.x) * 0.5, (windowHeight - board.y) * 0.5, board.x, board.y)
  }
  if (states.outro === true) {
    fill(colors.black)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.2, board.x * 0.95, board.y * 0.55)

    textAlign(CENTER, CENTER)
    fill(colors.white)
    noStroke()
    textFont(texts.font[0])
    textSize(board.x * 0.042)
    text(texts.note, windowWidth * 0.5 + board.x * 0.02, windowHeight * 0.5 - board.y * 0.39, board.x, board.y)

    fill(colors.black)
    noStroke()
    textFont(texts.font[1])
    textSize(board.x * 0.030)
    text(texts.desc, windowWidth * 0.5, windowHeight * 0.5 + board.y * 0.175, board.x, board.y)

    if (mouseX >= windowWidth * 0.5 - board.x * 0.125 && mouseX <= windowWidth * 0.5 + board.x * 0.125 && mouseY >= windowHeight * 0.5 + board.y * 0.4 - board.x * 0.021 && mouseY <= windowHeight * 0.5 + board.y * 0.4 + board.x * 0.021) {
      fill(colors.white)
    } else {
      fill(colors.black)
    }
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5 + board.y * 0.4, board.x * 0.25, board.x * 0.042)

    textFont(texts.font[1])
    textSize(board.x * 0.024)
    if (mouseX >= windowWidth * 0.5 - board.x * 0.125 && mouseX <= windowWidth * 0.5 + board.x * 0.125 && mouseY >= windowHeight * 0.5 + board.y * 0.4 - board.x * 0.021 && mouseY <= windowHeight * 0.5 + board.y * 0.4 + board.x * 0.021) {
      fill(colors.black)
    } else {
      fill(colors.white)
    }
    noStroke()
    text(texts.down, windowWidth * 0.5, windowHeight * 0.5 + board.y * 0.4 - board.x * 0.0045, board.x, board.y)
  }
}

function mousePressed() {
  if (states.intro === true) {
    setTimeout(function() {
      states.intro = false
      states.outro = true
    }, 200)
  }
  if (states.outro === true) {
    if (mouseX >= windowWidth * 0.5 - board.x * 0.125 && mouseX <= windowWidth * 0.5 + board.x * 0.125 && mouseY >= windowHeight * 0.5 + board.y * 0.4 - board.x * 0.021 && mouseY <= windowHeight * 0.5 + board.y * 0.4 + board.x * 0.021) {
      window.location.replace('https://drive.google.com/drive/folders/1HYj3gygCqpgXCXT9kfSj6cXySo1agqWi?usp=sharing')
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight * (16 / 9)) {
    board.x = windowHeight * (16 / 9)
    board.y = windowHeight
  } else {
    board.x = windowWidth
    board.y = windowWidth * (9 / 16)
  }
  createCanvas(windowWidth, windowHeight)
}
